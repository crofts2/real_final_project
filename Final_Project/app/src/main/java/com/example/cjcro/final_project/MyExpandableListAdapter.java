package com.example.cjcro.final_project;

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by cjcro on 11/15/2017.
 */

public class MyExpandableListAdapter extends BaseExpandableListAdapter  {
    private Context context;
    private ArrayList<ParentRow> parentRowList;
    private ArrayList<ParentRow> originalList;


    public MyExpandableListAdapter(Context context, ArrayList<ParentRow> originalList){
        this.context = context;
        this.parentRowList = new ArrayList<>();
        this.parentRowList.addAll(originalList);
        this.originalList= new ArrayList<>();
        this.originalList.addAll(originalList);
    }

    @Override
    public int getGroupCount() {
        return parentRowList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parentRowList.get(i).getChildList().size();
    }

    @Override
    public Object getGroup(int i) {
        return parentRowList.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return parentRowList.get(i).getChildList().get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    /*
    This function generates the group view for search items being displayed
     */
    @Override
    public View getGroupView(int groupItem, boolean b, View view, ViewGroup viewGroup) {
        ParentRow parentRow = (ParentRow) getGroup(groupItem);

        if(view== null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.parent_row, null);
        }

        TextView heading = (TextView) view.findViewById(R.id.parent_text);

        heading.setText(parentRow.getName().trim());
        return view;
    }

    /*
    This function displays the the child items (specific search items)
     */
    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        ChildRow childRow = (ChildRow) getChild(i,i1);
        if(view == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.child_row, null);
        }
        ImageView childIcon = (ImageView) view.findViewById(R.id.child_icon);
        childIcon.setImageResource(R.drawable.black_shirt);

        final TextView childText = (TextView) view.findViewById(R.id.child_text);
        childText.setText(childRow.getText().trim());

        final View finalConvertView = view;
        childText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(finalConvertView.getContext(),"display clothing items in content view",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(),Main2Activity.class);
                view.getContext().startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    /*
    As input is being put into the search bar, this function will filter out data that
    does not pertain to our search.
     */
    public void filterData(String query){
       query = query.toLowerCase();
        parentRowList.clear();

        if(query.isEmpty()){
            parentRowList.addAll(originalList);
        }
        else {
            for(ParentRow parentRow : originalList) {
                ArrayList<ChildRow> childList = parentRow.getChildList();
                ArrayList<ChildRow> newList = new ArrayList<>();

                for (ChildRow childRow : childList) {
                    if (childRow.getText().toLowerCase().contains(query)) {
                        newList.add(childRow);
                    }
                }//end for child
                if (newList.size() > 0) {
                    ParentRow nParentRow = new ParentRow(parentRow.getName(), newList);
                    parentRowList.add(nParentRow);
                }
            }//end for parent
        }//end else
        notifyDataSetChanged();
    }
}
