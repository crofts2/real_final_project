package com.example.cjcro.final_project;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.SearchView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.cjcro.final_project.R.*;
import static com.example.cjcro.final_project.R.layout.*;

public class MainActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    //Variables for search functionality
    private SearchManager searchManager;
    private android.widget.SearchView searchView;
    private MyExpandableListAdapter listAdapter;
    private ExpandableListView myList;
    private ArrayList<ParentRow> parentList = new ArrayList<ParentRow>();
    private ArrayList<ParentRow> showTheseParentList = new ArrayList<ParentRow>();
    private MenuItem searchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);
        Toolbar toolbar = (Toolbar) findViewById(id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        parentList = new ArrayList<ParentRow>();
        showTheseParentList = new ArrayList<ParentRow>();

        //Displays the search list
        displayList();

        //Expands list of contents
        expandAll();
    }

    private void loadData() {
        //First Section loads shirt data in from string returned by JSON Parsing function
        ArrayList<ChildRow> childRows = new ArrayList<ChildRow>();
        ParentRow parentRow = null;
        String shirts = loadShirtJSONFromAsset();
        List<String> shirtsList = Arrays.asList(shirts.split("\\s*,\\s*"));
        for(int i = 0; i < shirtsList.size() ; i++) {
            String temp = shirtsList.get(i);
            temp.replace(": $", ":$");
            temp = temp.replace("{\"", "");
            temp = temp.replace("}", "");
            temp = temp.replace("\"", "");
            temp = temp.replace("\'", "");
            childRows.add(new ChildRow(drawable.black_shirt, temp));
        }
        TextView text_view = (TextView) findViewById(id.text_view_clothes);
        //text_view.setText(childRows.get(0).getText());
        parentRow = new ParentRow("List of Shirts", childRows);
        parentList.add(parentRow);

        //-------------------------------------------------------------------------------------
        //Second Section loads hat data in from string returned by JSON Parsing function
        childRows = new ArrayList<ChildRow>();
        String hats = loadHatJSONFromAsset();
        List<String> hatsList = Arrays.asList(hats.split("\\s*,\\s*"));
        for(int i = 0; i < hatsList.size() ; i++) {
            String temp = hatsList.get(i);
            temp.replace(": $", ":$");
            temp = temp.replace("{\"", "");
            temp = temp.replace("}", "");
            temp = temp.replace("\"", "");
            temp = temp.replace("\'", "");
            childRows.add(new ChildRow(drawable.ic_menu_camera, temp));
        }

        parentRow = new ParentRow("List of Hats", childRows);
        parentList.add(parentRow);

        //-------------------------------------------------------------------------------------

        //Third section loads pants data in from string returned by JSON Parsing function
        childRows = new ArrayList<ChildRow>();
        String pants = loadPantsJSONFromAsset();
        List<String> pantsList = Arrays.asList(pants.split("\\s*,\\s*"));
        for(int i = 0; i < pantsList.size() ; i++) {
            String temp = pantsList.get(i);
            temp.replace(": $", ":$");
            temp = temp.replace("{\"", "");
            temp = temp.replace("}", "");
            temp = temp.replace("\"", "");
            temp = temp.replace("\'", "");
            childRows.add(new ChildRow(drawable.black_pants, temp));
        }
        parentRow = new ParentRow("List of Pants", childRows);
        parentList.add(parentRow);

    }
    private void expandAll(){
        int count = listAdapter.getGroupCount();
        for(int i = 0 ; i < count; i++ ){
            myList.expandGroup(i);
        }
    }

    private void displayList(){
        loadData();

        myList = (ExpandableListView) findViewById(R.id.expandableListView_search);
        listAdapter = new MyExpandableListAdapter(MainActivity.this, parentList);
        myList.setAdapter(listAdapter);
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        searchItem = menu.findItem(id.action_search);
        searchView = (SearchView) searchItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.requestFocus();

        return true;
    }


    @Override
    public boolean onClose() {
        listAdapter.filterData("");
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        listAdapter.filterData(s);
        expandAll();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        listAdapter.filterData(s);
        expandAll();

        return false;
    }
    /*
    This Function takes opens a JSON File and converts it to a string that
    I can use to populate a ListView within the app.
     */
    public String loadHatJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("jsonDataHats.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    public String loadShirtJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("jsonDataShirts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    public String loadPantsJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("jsonDataPants.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
