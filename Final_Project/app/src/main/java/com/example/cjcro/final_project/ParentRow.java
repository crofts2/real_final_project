package com.example.cjcro.final_project;

import java.util.ArrayList;

/**
 * Created by cjcro on 11/15/2017.
 */

public class ParentRow {
    private String name;
    private ArrayList<ChildRow> childList;

    public ParentRow(String name, ArrayList<ChildRow> childList) {
        this.name = name;
        this.childList = childList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setChildList(ArrayList<ChildRow> childList) {
        this.childList = childList;
    }

    public String getName() {
        return name;
    }

    public ArrayList<ChildRow> getChildList() {
        return childList;
    }
}
