from bs4 import BeautifulSoup as Soup
from urllib.request import urlopen as u_req
import logging
import json

"""""""""
TODO: Implement stored items into dictionary with Key = Item Name,
and Value = Item Price. **Also add value "tag" for more searchability
within the Android App itself.
"""


def get_black_shirts():
    black_shirts_dict = {}
    black_shirts = []
    black_shirt_prices = []

    # Specify filemode='w' to overwrite log file each time the function is executed
    logging.basicConfig(filename='output.log', filemode='w', level=logging.INFO)
    logging.info("This is logging info for the scraper")

    # For Week 2 I will only be searching Urban Outfitters
    blackshirt_url = 'https://www.urbanoutfitters.com/search?q=black+shirts'
    u_client = u_req(blackshirt_url)
    page_html = u_client.read()
    u_client.close()
    page_soup = Soup(page_html, "html.parser")

    # Populates container with products found in the soup
    container = page_soup.findAll("div", {"class": "dom-product-tile"})
    # Populates price_container with the corresponding product prices
    price_container = page_soup.findAll("span", {"class": "c-product-meta__current-price"})

    for i in container:
        # Holds item name
        shirt_item = i.get('data-qa-product')
        black_shirts.append(shirt_item)
        logging.info("Shirt Successfully added: " + shirt_item)

    for i in price_container:
        price_item = i.text
        # Parses the price to remove unnecessary information
        refined_price = price_item[-7:]
        black_shirt_prices.append(refined_price)
        logging.info("Price for item added successfully.")

    for i in range(len(black_shirts)):
        black_shirts_dict[black_shirts[i]] = black_shirt_prices[i]

    # Outputs the shirts into a file in the JSON Format
    with open('jsonDataShirts.json', 'w') as outfile:
        json.dump(black_shirts_dict, outfile, ensure_ascii=False)

    print(black_shirts_dict)


"""""""""
def get_black_shirts_amazon():
    black_shirts = []
    black_shirt_prices = []

    # Specify filemode='w' to overwrite log file each time the function is executed
    logging.basicConfig(filename='output.log', filemode='w', level=logging.INFO)
    logging.info("This is logging info for the scraper")

    # For Week 2 I will only be searching Urban Outfitters
    blackshirt_url = 'https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=black+shirts'
    u_client = u_req(blackshirt_url)
    page_html = u_client.read()
    u_client.close()
    page_soup = Soup(page_html, "html.parser")

    # Populates container with products found in the soup
    container = page_soup.findAll("li", {"class": "gc.u-center.item"})
    # Populates price_container with the corresponding product prices
    price_container = page_soup.findAll("span", {"class": "npr-3DFlx"})

    for i in container:
        # Holds item name
        shirt_item = i.get('')
        black_shirts.append(shirt_item)
        logging.info("Shirt Successfully added: " + shirt_item)

    for i in price_container:
        price_item = i.text
        # Parses the price to remove unnecessary information
        refined_price = price_item
        black_shirt_prices.append(refined_price)
        logging.info("Price for item added successfully.")

    # Outputs the shirts into a file in the JSON Format
    with open('jsonDataShirts.txt', 'w') as outfile:
        json.dump(black_shirts, outfile, ensure_ascii=False)

    print(black_shirts)
    print(black_shirt_prices)

"""


def get_black_pants():
        black_pants_dict = {}
        black_pants = []
        black_pants_prices = []

        # Specify filemode='w' to overwrite log file each time the function is executed
        logging.basicConfig(filename='output.log', filemode='w', level=logging.INFO)
        logging.info("This is logging info for the scraper")

        # For Week 2 I will only be searching Urban Outfitters
        blackpants_url = 'https://www.urbanoutfitters.com/search?q=black+pants'
        u_client = u_req(blackpants_url)
        page_html = u_client.read()
        u_client.close()
        page_soup = Soup(page_html, "html.parser")

        # Populates container with products found in the soup
        container = page_soup.findAll("div", {"class": "dom-product-tile"})
        # Populates price_container with the corresponding product prices
        price_container = page_soup.findAll("span", {"class": "c-product-meta__current-price"})

        for i in container:
            # Holds item name
            shirt_item = i.get('data-qa-product')
            black_pants.append(shirt_item)
            logging.info("Pants Successfully added: " + shirt_item)

        for i in price_container:
            price_item = i.text
            # Parses the price to remove unnecessary information
            refined_price = price_item[-7:]
            black_pants_prices.append(refined_price)
            logging.info("Price for item added successfully.")

        for i in range(len(black_pants)):
            black_pants_dict[black_pants[i]] = black_pants_prices[i]

        # Outputs the pants into a file in the JSON Format
        with open('jsonDataPants.json', 'w') as outfile:
            json.dump(black_pants_dict, outfile, ensure_ascii=False)

        # print(black_pants)
        # print(black_pants_prices)
        print(black_pants_dict)


def get_black_hats():
    black_hats_dict = {}
    black_hats = []
    black_hats_prices = []

    # Specify filemode='w' to overwrite log file each time the function is executed
    logging.basicConfig(filename='output.log', filemode='w', level=logging.INFO)
    logging.info("This is logging info for the scraper")

    # For Week 2 I will only be searching Urban Outfitters
    blackhats_url = 'https://www.urbanoutfitters.com/search?q=black+hats'
    u_client = u_req(blackhats_url)
    page_html = u_client.read()
    u_client.close()
    page_soup = Soup(page_html, "html.parser")

    # Populates container with products found in the soup
    container = page_soup.findAll("div", {"class": "dom-product-tile"})
    # Populates price_container with the corresponding product prices
    price_container = page_soup.findAll("span", {"class": "c-product-meta__current-price"})

    for i in container:
        # Holds item name
        hat_item = i.get('data-qa-product')
        black_hats.append(hat_item)
        logging.info("Hat Successfully added: " + hat_item)

    for i in price_container:
        price_item = i.text
        # Parses the price to remove unnecessary information
        refined_price = price_item[-7:]
        black_hats_prices.append(refined_price)
        logging.info("Price for item added successfully.")

    for i in range(len(black_hats)):
        black_hats_dict[black_hats[i]] = black_hats_prices[i]

    # Outputs the pants into a file in the JSON Format
    with open('jsonDataHats.json', 'w') as outfile:
        json.dump(black_hats_dict, outfile, ensure_ascii=False)

    print(black_hats_dict)


get_black_pants()
get_black_shirts()
get_black_hats()
# get_black_shirts_amazon()
